FROM python:3.8

# Install requirements before copying script to speddup build.
COPY requirements.txt /app/
RUN pip install -r /app/requirements.txt

COPY . /app

# Use like
ENTRYPOINT ["/app/run.sh"]